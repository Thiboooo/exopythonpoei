# **************************************************************************************************************
#                                                                                                              *
#                                                                                                              *
#                                                  Les variables                                               *
#                                                                                                              *
#                                                                                                              *
# **************************************************************************************************************
print("*********************************************")
print("*************** Les variables ***************")
print("*********************************************")
# Exercice 1 : Declarer une variable couleur avec la valeur 'verte'
couleur = "verte"
# Modifier la valeur de la variable couleur à jaune
couleur = "jaune"
# Afficher la valeur de la variable avec la fonction print
print(couleur)





# **************************************************************************************************************
#                                                                                                              *
#                                                                                                              *
#                                                  Les types                                                   *
#                                                                                                              *
#                                                                                                              *
# **************************************************************************************************************
print()
print("*********************************************")
print("*************** Les types *******************")
print("*********************************************")
#Exercice 1 : 

#Ecrivez votre nom dans une variable appelée nom.
nom = "Branlant"
#Utilisez la fonction print pour afficher le contenu de la varibale nom dans la console.
print(nom)

x = 5
y = 6

#Stockez le résulat de x + y dans une variable notée z.
z = x + y
#Affichez le résultat de la variable z dans la console à l'aide de la fonction print
print(z)
#utiliser la fonction type() pour afficher le type de z et de nom
print(type(z))



# **************************************************************************************************************
#                                                                                                              *
#                                                                                                              *
#                                                  Les listes                                                  *
#                                                                                                              *
#                                                                                                              *
# **************************************************************************************************************
print("************* Les listes *************")

liste_nombres = [1, 6, 98, 52, 1045, 3]

# 1) classez la liste, avec sort
liste_nombres.sort()
print(liste_nombres)

# 2) supprimez le premier élément de la liste, avec pop
liste_nombres.pop(0)
print(liste_nombres)

# 3) ajoutez le nombre "1097" à la fin de la liste, avec append
liste_nombres.append(1097)
print(liste_nombres)

# 4) récupérez le deuxième élément dans une variable "deuxieme_element"
deuxieme_element = liste_nombres[1]
print(deuxieme_element) # la console devrait afficher "6" !

# 5) affichez la longueur de la liste

print(len(liste_nombres))

# **************************************************************************************************************
#                                                                                                              *
#                                                                                                              *
#                                                  Les dictionnaires                                           *
#                                                                                                              *
#                                                                                                              *
# **************************************************************************************************************
print("************* Les dictionnaires *************")
# 1) Créez une variable de type dictionnaire appelée "chaussure"
chaussure = {}

""" 2) Ajoutez les éléments suivants dans le dictionnaire :
   - clef "taille" avec la valeur 42
   - clef "marque" avec la valeur "Nike"
   - clef "race" avec la valeur "berger-allemand"
"""
chaussure.update({"taille": 42,"marque":'Nike',"race":'berger-allemand'})

# 3) On s'est trompés ! Supprimez la clef "race" du dictionnaire :)
del chaussure['race']
# 4) Récupérez la taille de la chaussure dans une variable appelée "taille"
print("la taille de la chaussure est ",chaussure["taille"])


# **************************************************************************************************************
#                                                                                                              *
#                                                                                                              *
#                                                  Les Conditions                                              *
#                                                                                                              *
#                                                                                                              *
# **************************************************************************************************************
print("************* Les Conditions *************")
# ********************************
#              Exo 1             *
# ********************************
# 1) Comprenez le code ci-dessous
a = True
b = False
c = True

if a and b:
    x = 5
elif not c:
    x = 4
elif a:
    x = 8
else:
    x = 7

# 2) Déduisez-en la valeur de x et assignez la dans la variable nommée "solution"

solution = 8
print("la solution est : ",solution)
# 3) Vérifiez votre intuition en faisant tourner le code
if a and b:
    x = 5
    print(x)
elif not c:
    x = 4
    print(x)
elif a:
    x = 8
    print(x)
else:
    x = 7
    print(x)

# ********************************
#              Exo 2             *
# ********************************
# 1) Comprenez le code ci-dessous
a = 3
b = 7
c = 5

vrai_ou_faux = (a < b or b != c) and c >= b

# 2) Déduisez-en la valeur de vrai_ou_faux et assignez la dans la variable nommée "solution"
solution = False
print("la solution est :",solution)

# 3) Vérifiez votre intuition en faisant tourner le code 

print("la solution est :",vrai_ou_faux)
# **************************************************************************************************************
#                                                                                                              *
#                                                                                                              *
#                                                  Les Boucles                                                 *
#                                                                                                              *
#                                                                                                              *
# **************************************************************************************************************
print("************* Les Boucles *************")
"""
Le but de l'exercice est de calculer la somme des 100 premiers entiers naturels !

Pour information : 
vous êtes sur les pas du célèbre mathématicien Gauss
https://fr.wikipedia.org/wiki/Somme_(arithm%C3%A9tique)
"""

# 1) Utilisez une boucle et la fonction "range" pour calculer la somme.
# Testez et récupérez le résultat en faisant tourner le code
res = 0
for i in range(1,101):
    res += i
print("la somme de 1 a 100 est : ",res)

# 2) Assignez le résultat obtenu dans la variable "solution" pour vérification

solution = res

# Ne touchez pas le print ci-dessous :)


print(f"{solution} est la bonne valeur de la somme !" if solution == (100 * 101) / 2 else "Raté")



# **************************************************************************************************************
#                                                                                                              *
#                                                                                                              *
#                                                  Les Fonctions                                               *
#                                                                                                              *
#                                                                                                              *
# **************************************************************************************************************
print("************* Les Fonctions *************")
# Vous allez créer une fonction permettant de calculer le produit d'une liste de nombres
# Ecrivez la fonction et testez en faisant tourner le code que le calcul est correct !
def produit_entiers(liste_entiers):
    # écrivez le code ici
    res = 1
    for i in liste_entiers:
        res *= i
    print(res)
    return res

# Ne touchez pas le code de vérification ci-dessous :)

assert 1 == produit_entiers([1, 1, 1])
assert 6 == produit_entiers([1, 2, 3])
assert 0 == produit_entiers([1, 2, 3, 90, 0])