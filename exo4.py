Formation = "Python"
Jours = 3

print(f"La formation {Formation} dure {Jours} jours")
print("La formation {Formation} dure {Jours} jours")

print("La formation", Formation, "dure", Jours, "jours")

print("La formation %s dure %d jours" % (Formation, Jours))