from colorama import Fore, Back, Style, init
init(autoreset=True)
plateau = [" " for _ in range(9)]
import sys

def afficher(plateau, joueur=None, nb_tour=None):
    print("\n")
    print(" " + plateau[0] + Fore.GREEN +" | " + plateau[1] + Fore.GREEN +" | " + plateau[2] + " ")
    print(Fore.GREEN +"---+---+---")
    print(" " + plateau[3] + Fore.GREEN +" | " + plateau[4] + Fore.GREEN +" | " + plateau[5] + " ")
    print(Fore.GREEN +"---+---+---")
    print(" " + plateau[6] + Fore.GREEN +" | " + plateau[7] + Fore.GREEN +" | " + plateau[8] + " ")
    if joueur:
        print(Fore.BLUE + "\n *********************************** \n")
        print(Fore.BLUE + "BRAVO à toi joueur", Fore.BLUE + joueur, Fore.BLUE + "tu as gagné la partie en", nb_tour, Fore.BLUE + "tours !")
        print(Fore.BLUE + "\n ***********************************")
        sys.exit()



def jeu():
    gagne = False
    compteur_tour = 0
    joueur1 = input("Inserer nom joueur 1 'X' :")
    joueur2 = input("Inserer nom joueur 2 'O' :")
    joueur_gagnant = ""
    joueur_afficher = ""
    choix = 0
    while ( gagne == False ):
        afficher(plateau,joueur_gagnant,compteur_tour)
        if (compteur_tour % 2 == 0):
            joueur_afficher = joueur1
        else :
            joueur_afficher = joueur2
        print("A toi de jouer , ",joueur_afficher," choisis une case de 1 a 9 : ")
        choix = int(input())
        while(choix not in [1,2,3,4,5,6,7,8,9] ):
            if (compteur_tour % 2 == 0):
                joueur_afficher = joueur1
            else :
                joueur_afficher = joueur2
            print("A toi de jouer , ",joueur_afficher," choisis une case de 1 a 9 : ")
            choix = int(input())
        compteur_temp = compteur_tour
        if (compteur_tour % 2 == 0 ):
            if (choix == 1 and (plateau[6] != "X" and plateau[6] != "O") ):
                plateau[6] = "X"
                compteur_tour += 1
            if (choix == 2 ):
                plateau[7] = "X"
                compteur_tour += 1
            if (choix == 3 ):
                plateau[8] = "X"
                compteur_tour += 1
            if (choix == 4 ):
                plateau[3] = "X"
                compteur_tour += 1
            if (choix == 5 ):
                plateau[4] = "X"
                compteur_tour += 1
            if (choix == 6 ):
                plateau[5] = "X"
                compteur_tour += 1
            if (choix == 7 ):
                plateau[0] = "X"
                compteur_tour += 1
            if (choix == 8 ):
                plateau[1] = "X"
                compteur_tour += 1
            if (choix == 9 ):
                plateau[2] = "X"
                compteur_tour += 1
        else : 
            if (choix == 1 and (plateau[6] != "X" and plateau[6] != "O") ):
                plateau[6] = "O"
                compteur_tour += 1
            if (choix == 2 and (plateau[7] != "X" and plateau[7] != "O") ):
                plateau[7] = "O"
                compteur_tour += 1
            if (choix == 3 and (plateau[8] != "X" and plateau[8] != "O") ):
                plateau[8] = "O"
                compteur_tour += 1
            if (choix == 4 and (plateau[3] != "X" and plateau[3] != "O") ):
                plateau[3] = "O"
                compteur_tour += 1
            if (choix == 5 and (plateau[4] != "X" and plateau[4] != "O") ):
                plateau[4] = "O"
                compteur_tour += 1
            if (choix == 6 and (plateau[5] != "X" and plateau[5] != "O") ):
                plateau[5] = "O"
                compteur_tour += 1
            if (choix == 7 and (plateau[0] != "X" and plateau[0] != "O") ):
                plateau[0] = "O"
                compteur_tour += 1
            if (choix == 8 and (plateau[1] != "X" and plateau[1] != "O") ):
                plateau[1] = "O"
                compteur_tour += 1
            if (choix == 9 and (plateau[2] != "X" and plateau[2] != "O") ):
                plateau[2] = "O"
                compteur_tour += 1
                
        if (compteur_temp == compteur_tour):
            print("Attention ne joues pas sur une case deja pleine !")
            
        if (
        #lignes
        (plateau[6] == plateau[7] == plateau[8] != " ") or
        (plateau[3] == plateau[4] == plateau[5] != " ") or
        (plateau[0] == plateau[1] == plateau[2] != " ") or
        #colonnes
        (plateau[2] == plateau[5] == plateau[8] != " ") or
        (plateau[1] == plateau[4] == plateau[7] != " ") or
        (plateau[0] == plateau[3] == plateau[6] != " ") or
        #diagonal
        (plateau[6] == plateau[4] == plateau[2] != " ") or
        (plateau[0] == plateau[4] == plateau[8] != " ") ):
            print("tu as gagné !")
            if (compteur_tour % 2 == 0):
                print("joueur2 dans txt")
                joueur_gagnant = joueur2
            else :
                print("joueur1 dans txt")
                joueur_gagnant = joueur1
            #afficher(plateau,True,compteur_tour)
        if (compteur_tour == 9 and joueur_gagnant == ""):
            print(Fore.BLUE + "\n *********************************** \n")
            print(Fore.BLUE + "Egaliter Dommage !")
            print(Fore.BLUE + "\n ***********************************")
            sys.exit()
    
jeu()