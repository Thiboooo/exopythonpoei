import sys

Liste = []
print("----------------------------------------------------------------------------")
print('- 1 : - Ajouter un élément à la liste de courses\n')
print('- 2 : - Retirer un élément de la liste de courses\n')
print('- 3 : - Afficher les éléments de la liste de courses\n')
print('- 4 : - Vider la liste de courses\n')
print('- 5 : - Quitter le programme \n')
arg = int(input('Entrez le choix de 1 a 5 : '))
while(arg != 0):
    while (arg not in [1,2,3,4,5,99]):
        print("----------------------------------------------------------------------------")
        print('- 1 : - Ajouter un élément à la liste de courses\n')
        print('- 2 : - Retirer un élément de la liste de courses\n')
        print('- 3 : - Afficher les éléments de la liste de courses\n')
        print('- 4 : - Vider la liste de courses\n')
        print('- 5 : - Quitter le programme \n')
        arg = int(input('Vous vous etes trompé , Entrez le choix de 1 a 5 : '))

    if arg == 1:
        produit = input('Ajout du produit :')
        Liste.append(produit)
        arg = 99
    elif arg == 2:
        produit = input('Suppression du produit :')
        Liste.remove(produit)
        arg = 99
    elif arg == 3:
        print('Affichage des produits :')
        for i in range(0,len(Liste)):
            print('Produit ',i+1,' :',Liste[i])
        arg = 99
    elif arg == 4:
        print('On vide la liste de courses :')
        Liste.clear()
        arg = 99
    elif arg == 5:
        print("bye bye !")
        sys.exit()
    elif arg == 99:
        print("----------------------------------------------------------------------------")
        print('- 1 : - Ajouter un élément à la liste de courses\n')
        print('- 2 : - Retirer un élément de la liste de courses\n')
        print('- 3 : - Afficher les éléments de la liste de courses\n')
        print('- 4 : - Vider la liste de courses\n')
        print('- 5 : - Quitter le programme \n')
        arg = int(input('Tu veux faire quoi ensuite , Entrez le choix de 1 a 5 : '))
        
