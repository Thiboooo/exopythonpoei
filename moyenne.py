print("Nous allons chercher à calculer la moyenne des notes d'un groupe d'élèves.")

#Utiliser la fonction input(), attention on cherche à avoir un int seulement
#On stock ce qu'on inscrit au clavier dans la variable ci dessous
Nb_etudiants = int(input("Entrez le nombre d'élèves :"))

#On crée une liste vide de nom Liste_Notes
Liste_Notes = []

#On parcourt le nombre d'étudiants
for eleve in range (0,Nb_etudiants) :
    #On affiche le message "Entrez la note numéro BLABLA"
    print("Entrez la note ",eleve+1," :")
    
    #la variable note contient la valeur entrée au clavier
    note = int(input("--> "))
    
    #On vérifie que la note n'est ni négative, ni supérieur à 20

    #Si les critères ne sont pas validés, on demande d'Entrer
    #un nombre ENTIER COMPRIS ENTRE 0 ET 20 
    while (note < 0 or note > 20):
        note = int(input("Veuillez entrer un nombre entier compris entre 0 et 20 :"))
    
    #Si les critères sont vérifiés, on ajoute la note à la liste définie plus haut
    Liste_Notes.append(note)
    
#On fait la somme de la liste
somme = sum(Liste_Notes)

#On calcule la moyenne ET L'AFFECTE à la variable moyenne
moyenne = somme/Nb_etudiants

print("-------------")
print("-------------")
print("Le nombre d'élèves est : ", Nb_etudiants)
print("La liste des notes obtenues est :", Liste_Notes)
print("La moyenne de la classe est : ", moyenne)
print("\U0001f600")