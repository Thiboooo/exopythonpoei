import random

#50 pc , 3 potions
joueur_pv = 50
joueur_potions = 3
ennemi_pv = 50
tour = 0
while(joueur_pv > 0 and ennemi_pv > 0):
    print("-----------------------------------------------------------------------------")
    print("\033[1m HP Joueur :",joueur_pv,"HP Ennemi ",ennemi_pv,"\033[0m")
    if ( tour % 2 == 0 ):
        print("Au tour du Joueur")
        print("Souhaitez-vous attaquer (1) ou utiliser une potion (2)")
        choix = int(input())
        if ( choix == 1 ) :
            dmg = random.randint(5, 10)
            ennemi_pv -= dmg
            print("le joueur inflige ",dmg," points de degats a l'ennemi, il lui reste ",ennemi_pv)
            tour += 1
        if ( choix == 2 ) :
            soin = random.randint(15,30)
            if (joueur_potions >= 1):
                joueur_potions -= 1
                joueur_pv += soin
                print("le joueur soigne de ",soin," points de vie , il lui reste ",joueur_potions," potions")
                tour += 1
            else :
                print("plus de potions !")
        
        if (ennemi_pv < 0):
            print("L'ennemi est KO , le Joueur a gagner")
    else :
        print("Au tour de L'ennemi")
        dmg = random.randint(5, 15)
        joueur_pv -= dmg
        print("l'ennemi inflige ",dmg," points de degats au joueur, il lui reste ",joueur_pv)
        tour += 1
        if (joueur_pv < 0):
            print("Le Joueur est KO , l'ennemi a gagner")
